import java.util.ArrayList;

// Výčtový typ pro směr pohybu
enum SmerPohybu {
    LEFT, RIGHT, UP, DOWN
}

// Třída pro simulaci pohybu po šachovnici
class HraPosunPoSachovnici {
    private int x;
    private int y;
    private int nx;
    private int ny;
    private ArrayList<String> historiePohybu;

    public HraPosunPoSachovnici(int nx, int ny) {
        this.nx = nx;
        this.ny = ny;
        this.x = 0;
        this.y = 0;
        this.historiePohybu = new ArrayList<>();
        this.historiePohybu.add("[0;0]");
    }

    public void provedPohyb(SmerPohybu smer) {
        switch (smer) {
            case LEFT:
                if (x > 0) {
                    x--;
                }
                break;
            case RIGHT:
                if (x < nx - 1) {
                    x++;
                }
                break;
            case UP:
                if (y > 0) {
                    y--;
                }
                break;
            case DOWN:
                if (y < ny - 1) {
                    y++;
                }
                break;
        }
        historiePohybu.add("[" + x + ";" + y + "]");
    }

    public String ziskejAktualniPolohu() {
        return "[" + x + ";" + y + "]";
    }

    public String[] ziskejHistoriiPohybu() {
        return historiePohybu.toArray(new String[0]);
    }
}
