import java.util.Scanner;

class RealizaceHryKonzola {
    private HraPosunPoSachovnici hra;

    public RealizaceHryKonzola(int nx, int ny) {
        this.hra = new HraPosunPoSachovnici(nx, ny);
    }

    public void spustHru() {
        Scanner scanner = new Scanner(System.in);
        char vstup = ' ';

        while (Character.toUpperCase(vstup) != 'K') {
            tiskniAktualniPolohu();
            System.out.println("Stiskněte <L> pro posun vlevo, <R> pro posun vpravo, <U> pro posun nahoru, <D> pro posun dolů, <K> pro ukončení:");
            vstup = scanner.next().charAt(0);
            zpracujVstup(vstup);
        }

        scanner.close();
    }

    public void tiskniAktualniPolohu() {
        System.out.println("Aktuální poloha: " + hra.ziskejAktualniPolohu());
    }

    public void tiskniHistoriiPohybu() {
        System.out.println("Historie pohybu:");
        String[] historie = hra.ziskejHistoriiPohybu();
        for (String pohyb : historie) {
            System.out.print(pohyb + " ");
        }
        System.out.println();
    }

    public void zpracujVstup(char vstup) {
        switch (Character.toUpperCase(vstup)) {
            case 'L':
                hra.provedPohyb(SmerPohybu.LEFT);
                break;
            case 'R':
                hra.provedPohyb(SmerPohybu.RIGHT);
                break;
            case 'U':
                hra.provedPohyb(SmerPohybu.UP);
                break;
            case 'D':
                hra.provedPohyb(SmerPohybu.DOWN);
                break;
            case 'K':
                System.out.println("Konec hry.");
                System.exit(0);
                break;
            default:
                System.out.println("Neplatný vstup, zadejte znovu.");
                break;
        }
        tiskniHistoriiPohybu();
    }
}