import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("=== Výpočet mocnin a odmocnin ===");
        mocnina_cisla(50.5);

        System.out.println("=== Goniometrické funkce ===");
        goniometricke_funkce(50);

        System.out.println("=== Pravoúhlý trojúhelník ===");
        trojuhelnik_pravouhly(50, 50);

        System.out.println("=== Pravoúhlý trojúhelník ===");
        trojuhelnik_pravouhly(50, 50);

        System.out.println("=== Platnost datumu ===");
        platnost_datumu();

        System.out.println("=== Aktuální datum ===");
        aktualni_datum();

        System.out.println("=== Obsah složky ===");
        String cesta = "/media/Data";
        zobraz_obsah_slozky(cesta);

        System.out.println("=== Práce se soubory ===");
        String cestaZkopirovaniPuvodni = "/home/user/Downloads/testFile";
        String cestaZkopirovaniNova = "/home/user/Downloads/testFile1";
        uprav_soubor("kopirovat", cestaZkopirovaniPuvodni, cestaZkopirovaniNova);
    }

    public static void mocnina_cisla(double cislo) {
        double druhaMocnina = Math.pow(cislo, 2);
        double tretiMocnina = Math.pow(cislo, 3);
        double druhaOdmocnina = Math.sqrt(cislo);
        double tretiOdmocnina = Math.cbrt(cislo);

        System.out.println("Druhá mocnina: " + druhaMocnina);
        System.out.println("Třetí mocnina: " + tretiMocnina);
        System.out.println("Druhá odmocnina: " + druhaOdmocnina);
        System.out.println("Třetí odmocnina: " + tretiOdmocnina);
    }

    public static void goniometricke_funkce(double uhel) {
        double uhelVRadianech = Math.toRadians(uhel);

        double sin = Math.sin(uhelVRadianech);
        double cos = Math.cos(uhelVRadianech);
        double tan = Math.tan(uhelVRadianech);
        double cotan = 1.0 / Math.tan(uhelVRadianech);

        System.out.println("Sinus: " + sin);
        System.out.println("Kosinus: " + cos);
        System.out.println("Tangens: " + tan);
        System.out.println("Kotangens: " + cotan);
    }

    public static void trojuhelnik_pravouhly(double odvesnaA, double odvesnaB) {
        double prepona = Math.sqrt(Math.pow(odvesnaA, 2) + Math.pow(odvesnaB, 2));
        double sinAlfa = odvesnaA / prepona;
        double sinBeta = odvesnaB / prepona;

        double uhelAlfaRad = Math.asin(sinAlfa);
        double uhelBetaRad = Math.asin(sinBeta);

        double uhelAlfa = Math.toDegrees(uhelAlfaRad);
        double uhelBeta = Math.toDegrees(uhelBetaRad);

        System.out.println("Přepona: " + prepona);
        System.out.println("Úhel alfa: " + uhelAlfa);
        System.out.println("Úhel beta: " + uhelBeta);
    }

    public static void platnost_datumu() {
        Scanner scanner = new Scanner(System.in);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        try {
            System.out.println("Zadejte první datum ve formátu dd.MM.yyyy:");
            Date prvniDatum = sdf.parse(scanner.nextLine());

            System.out.println("Zadejte druhé datum ve formátu dd.MM.yyyy:");
            Date druheDatum = sdf.parse(scanner.nextLine());

            if (druheDatum.after(prvniDatum)) {
                System.out.println("Druhé datum je pozdější než první.");
            }

            else {
                System.out.println("Druhé datum není pozdější než první.");
            }
        }

        catch (ParseException e) {
            System.out.println("Chybný formát data.");
        }
    }

    public static void aktualni_datum() {
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        String formattedDate = dateFormat.format(currentDate);
        System.out.println("Aktuální datum a čas: " + formattedDate);
    }

    public static void zobraz_obsah_slozky(String cestaKeSlozce) {
        File slozka = new File(cestaKeSlozce);

        if (slozka.exists() && slozka.isDirectory()) {
            File[] soubory = slozka.listFiles();

            if (soubory != null) {
                System.out.println("Obsah složky " + cestaKeSlozce + ":");

                for (File soubor : soubory) {
                    System.out.println(soubor.getName());
                }
            }

            else {
                System.out.println("Složka je prázdná.");
            }
        }

        else {
            System.out.println("Zadaná cesta není platná složka nebo neexistuje.");
        }
    }

    public static void uprav_soubor(String akce, String zdrojovaCesta, String... volitelneParametry) {
        Path zdroj = Paths.get(zdrojovaCesta);

        try {
            switch (akce) {
                case "kopirovat":
                    if (volitelneParametry.length > 0) {
                        Path cil = Paths.get(volitelneParametry[0]);
                        Path cilovySoubor = cil.resolve(zdroj.getFileName());

                        if (Files.exists(cilovySoubor)) {
                            System.out.println("Soubor/adresář již existuje na cílové cestě");
                        }

                        else {
                            Files.copy(zdroj, cil.resolve(zdroj.getFileName()), StandardCopyOption.REPLACE_EXISTING);
                            System.out.println("Soubor/adresář byl zkopírován.");
                        }
                    }

                    else {
                        System.out.println("Chybějící parametr cilovaCesta");
                    }
                    break;

                case "odstranit":
                    Files.delete(zdroj);
                    System.out.println("Soubor/adresář byl odstraněn");
                    break;

                case "prejmenovat":
                    if (volitelneParametry.length > 0) {
                        Path novaCesta = Paths.get(volitelneParametry[0] + "/" + zdroj.getFileName());
                        Files.move(zdroj, novaCesta, StandardCopyOption.REPLACE_EXISTING);
                        System.out.println("Soubor/adresář byl přejmenován.");
                    }

                    else {
                        System.out.println("Chybějící parametr cilovaCesta");
                    }
                    break;

                default:
                    System.out.println("Neplatná akce");
                    break;
            }
        }

        catch (IOException e) {
            System.out.println("Nastala chyba při operaci se souborem/adresářem: " + e.getMessage());
        }
    }
}
