import java.io.*;

public class BinaryFileReadWrite {
    public static void main(String[] args) {
        // Zápis hodnot do binárního souboru
        try  {
            DataOutputStream dos = new DataOutputStream(new FileOutputStream("data.bin"));
            int intValue = 420;
            long longValue = 25696969;

            dos.writeInt(intValue);
            dos.writeLong(longValue);
        }

        catch (IOException e) {
            System.out.println("Data se nepodařilo zapsat");
        }

        // Čtení hodnot z binárního souboru
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream("data.bin"));
            // Čtení int a long v pořadí int -> long
            int readInt = dis.readInt();
            long readLong = dis.readLong();

            System.out.println("Čtení v pořadí int -> long:");
            System.out.println("Přečteno int: " + readInt);
            System.out.println("Přečteno long: " + readLong);

            dis.close();
            DataInputStream dis2 = new DataInputStream(new FileInputStream("data.bin"));

            // Čtení long a int v pořadí long -> int
            long readLong2 = dis2.readLong();
            int readInt2 = dis2.readInt();

            System.out.println("\nČtení v pořadí long -> int:");
            System.out.println("Přečteno long: " + readLong2);
            System.out.println("Přečteno int: " + readInt2);

            dis2.close();
        }

        catch (IOException e) {
            System.out.println("Nepodařilo se přečíst data");
        }

        // Velikost souboru na disku
        File file = new File("data.bin");
        long fileSize = file.length();
        System.out.println("\nVelikost souboru na disku: " + fileSize + " bajtů");
    }
}
