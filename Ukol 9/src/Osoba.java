import java.io.Serializable;

// Třída Osoba reprezentující osobu
class Osoba implements Serializable {
    private String jmeno;
    private String prijmeni;
    private int vek;
    private Datum datumNarozeni;

    // Konstruktor třídy Osoba
    public Osoba(String jmeno, String prijmeni, int vek, Datum datumNarozeni) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.vek = vek;
        this.datumNarozeni = datumNarozeni;
    }


    // Metoda toString pro zobrazení informací o osobě
    @Override
    public String toString() {
        return "Jméno: " + jmeno + ", Příjmení: " + prijmeni + ", Věk: " + vek + ", Datum narození: " + datumNarozeni.toString();
    }
}
