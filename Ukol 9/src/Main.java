public class Main {
    public static void main(String[] args) {
        new BinaryFileReadWrite();
        IOFileTool FT = new IOFileTool();
        int pocetZnaku = FT.pocetZnakuFile("data.bin");
        System.out.println("Počet znaků v souboru data.bin je: " + pocetZnaku);

        FT.copyFile("data.bin", "copy.bin");

        Datum datum = new Datum(10, 12, 2000);
        Osoba osoba = new Osoba("Jan", "Novák", 30, datum);

        // Serializace objektu Osoba do souboru
        IOFileTool.osoba_serializace(osoba, "osoba_serialized.ser");

        // Deserializace objektu Osoba ze souboru
        Osoba deserializedOsoba = IOFileTool.osoba_deserializace("osoba_serialized.ser");
        if (deserializedOsoba != null) {
            System.out.println("Deserializovaný objekt Osoba: " + deserializedOsoba);
        }

        // Zápis dat instance Datum do binárního souboru
        IOFileTool.write_datum_filebin(datum, "datum.bin");

        // Čtení dat instance Datum z binárního souboru
        Datum readDatum = IOFileTool.read_datum_filebin("datum.bin");
        if (readDatum != null) {
            System.out.println("Načtená instance Datum: " + readDatum.toString());
        }
    }
}