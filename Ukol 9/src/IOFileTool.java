import java.io.*;

// Třída IOFileTool pro práci se soubory
public class IOFileTool {
    public int pocetZnakuFile(String filename) {
        int pocetZnaku = 0;

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            int znak;
            while ((znak = reader.read()) != -1) {
                pocetZnaku++;
            }
        }

        catch (IOException e) {
            System.out.println("Nepodařilo se číst ze souboru");
        }

        return pocetZnaku;
    }

    public void copyFile(String filename1, String filename2) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename1));
             BufferedWriter writer = new BufferedWriter(new FileWriter(filename2))) {

            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.isEmpty()) {
                    writer.write(line);
                    writer.newLine();
                }
            }
        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Metoda pro serializaci objektu Osoba do souboru
    public static void osoba_serializace(Osoba osoba, String filename) {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(filename))) {
            outputStream.writeObject(osoba);
            System.out.println("Objekt Osoba byl serializován do souboru " + filename);
        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Metoda pro deserializaci objektu Osoba ze souboru
    public static Osoba osoba_deserializace(String filename) {
        Osoba osoba = null;
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(filename))) {
            osoba = (Osoba) inputStream.readObject();
            System.out.println("Objekt Osoba byl deserializován ze souboru " + filename);
        }

        catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return osoba;
    }

    // Metoda pro zápis dat instance Datum do binárního souboru
    public static void write_datum_filebin(Datum datum, String filename) {
        try (DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(filename))) {
            outputStream.writeInt(datum.getDen());
            outputStream.writeInt(datum.getMesic());
            outputStream.writeInt(datum.getRok());
            System.out.println("Údaje instance Datum byly zapsány do binárního souboru " + filename);
        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Metoda pro čtení dat instance Datum z binárního souboru a vytvoření instance Datum
    public static Datum read_datum_filebin(String filename) {
        Datum datum = null;
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream(filename))) {
            int den = inputStream.readInt();
            int mesic = inputStream.readInt();
            int rok = inputStream.readInt();
            datum = new Datum(den, mesic, rok);
            System.out.println("Údaje instance Datum byly načteny z binárního souboru " + filename);
        }

        catch (IOException e) {
            e.printStackTrace();
        }
        return datum;
    }
}
