package MojeMatematickaKnihovna;
public class KvadratickaRovniceVypocty {
    public static double vypocetFx(double a, double b, double c, double x) {
        return a * x * x + b * x + c;
    }

    public static int pocetKorenu(double a, double b, double c) {
        double diskriminant = b * b - 4 * a * c;
        if (diskriminant > 0) {
            return 2;
        }

        else if (diskriminant == 0) {
            return 1;
        }

        else {
            return 0;
        }
    }

    public static double[] hodnotyKorenu(double a, double b, double c) {
        double diskriminant = b * b - 4 * a * c;
        double[] koreny = new double[2];

        if (diskriminant > 0) {
            koreny[0] = (-b + Math.sqrt(diskriminant)) / (2 * a);
            koreny[1] = (-b - Math.sqrt(diskriminant)) / (2 * a);
        }

        else {
            koreny[0] = -b / (2 * a);
        }

        return koreny;
    }
}
