public class Main {
    public static void main(String[] args) {
        System.out.println("=== Kvadraticka rovnice ===");
        testKvadraticka();

        System.out.println("=== Matematicka knihovna ===");
        testMojeMath();
    }

    public static void testKvadraticka() {
        KvadratickaRovnice rovnice = new KvadratickaRovnice(2, 5, -3);

        System.out.println("Hodnota parametru a: " + rovnice.getA());
        System.out.println("Hodnota parametru b: " + rovnice.getB());
        System.out.println("Hodnota parametru c: " + rovnice.getC());
        System.out.println("Hodnota f(x) pro x = 4: " + rovnice.vypocetFx(4));

        rovnice.setA(3);
        System.out.println("Nová hodnota parametru a: " + rovnice.getA());

        double vysledekFx = rovnice.vypocetFx(4);
        System.out.println("Hodnota f(x) pro x = 4: " + vysledekFx);

        int pocetKorenu = rovnice.pocetKorenu();
        System.out.println("Počet kořenů kvadratické rovnice: " + pocetKorenu);

        double[] koreny = rovnice.hodnotyKorenu();
        System.out.println("Hodnoty kořenů kvadratické rovnice:");
        for (double koren : koreny) {
            System.out.println(koren);
        }
    }

    public static void testMojeMath() {
        int[] celeCisla = {1, 2, 3, 4, 5};
        double[] realnaCisla = {1.5, 2.5, 3.5, 4.5, 5.5};

        // Výpočet průměru a rozptylu pro celá čísla
        double prumerCele = MojeMath.prumer(celeCisla);
        double rozptylCele = MojeMath.rozptyl(celeCisla);
        System.out.println("Průměr celých čísel: " + prumerCele);
        System.out.println("Rozptyl celých čísel: " + rozptylCele);

        // Výpočet průměru a rozptylu pro reálná čísla
        double prumerRealna = MojeMath.prumer(realnaCisla);
        double rozptylRealna = MojeMath.rozptyl(realnaCisla);
        System.out.println("Průměr reálných čísel: " + prumerRealna);
        System.out.println("Rozptyl reálných čísel: " + rozptylRealna);

        // Výpočet průměru a rozptylu pro celá čísla prostřednictvím výstupních parametrů
        double[] vysledkyCele = new double[2];
        MojeMath.prumer_a_rozptyl(celeCisla, vysledkyCele);
        System.out.println("Průměr celých čísel (parametry): " + vysledkyCele[0]);
        System.out.println("Rozptyl celých čísel (parametry): " + vysledkyCele[1]);

        // Výpočet průměru a rozptylu pro reálná čísla prostřednictvím výstupních parametrů
        double[] vysledkyRealna = new double[2];
        MojeMath.prumer_a_rozptyl(realnaCisla, vysledkyRealna);
        System.out.println("Průměr reálných čísel (parametry): " + vysledkyRealna[0]);
        System.out.println("Rozptyl reálných čísel (parametry): " + vysledkyRealna[1]);
    }
}
