public class MojeMath {
    public static double prumer(int[] cisla) {
        int suma = 0;
        for (int cislo : cisla) {
            suma += cislo;
        }
        return (double) suma / cisla.length;
    }

    public static double prumer(double[] cisla) {
        double suma = 0;
        for (double cislo : cisla) {
            suma += cislo;
        }
        return suma / cisla.length;
    }

    public static double rozptyl(int[] cisla) {
        double prum = prumer(cisla);
        double soucetKvadratuRozdilu = 0;
        for (int cislo : cisla) {
            soucetKvadratuRozdilu += Math.pow(cislo - prum, 2);
        }
        return soucetKvadratuRozdilu / cisla.length;
    }

    public static double rozptyl(double[] cisla) {
        double prum = prumer(cisla);
        double soucetKvadratuRozdilu = 0;
        for (double cislo : cisla) {
            soucetKvadratuRozdilu += Math.pow(cislo - prum, 2);
        }
        return soucetKvadratuRozdilu / cisla.length;
    }

    // Metoda pro výpočet průměru a rozptylu celých čísel prostřednictvím výstupních parametrů
    public static void prumer_a_rozptyl(int[] cisla, double[] vysledky) {
        vysledky[0] = prumer(cisla);
        vysledky[1] = rozptyl(cisla);
    }

    // Metoda pro výpočet průměru a rozptylu reálných čísel prostřednictvím výstupních parametrů
    public static void prumer_a_rozptyl(double[] cisla, double[] vysledky) {
        vysledky[0] = prumer(cisla);
        vysledky[1] = rozptyl(cisla);
    }
}
