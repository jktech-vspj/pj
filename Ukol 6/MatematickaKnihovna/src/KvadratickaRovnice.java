import MojeMatematickaKnihovna.KvadratickaRovniceVypocty;

public class KvadratickaRovnice {
    private double a;
    private double b;
    private double c;

    public KvadratickaRovnice(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double vypocetFx(double x) {
        return KvadratickaRovniceVypocty.vypocetFx(a, b, c, x);
    }

    public int pocetKorenu() {
        return KvadratickaRovniceVypocty.pocetKorenu(a, b, c);
    }

    public double[] hodnotyKorenu() {
        return KvadratickaRovniceVypocty.hodnotyKorenu(a, b, c);
    }
}
