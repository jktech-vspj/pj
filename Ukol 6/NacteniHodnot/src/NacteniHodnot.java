import java.util.Scanner; // Importujeme Scanner pro načítání vstupu od uživatele

// Třída NacteniHodnot
public class NacteniHodnot {
    public static int nactiHodnoty() {
        Scanner scanner = new Scanner(System.in); // Inicializace Scanneru pro načítání vstupu
        System.out.println("Zadejte hodnotu: ");
        int hodnota = scanner.nextInt(); // Načtení celočíselné hodnoty od uživatele

        return hodnota;
    }
}
