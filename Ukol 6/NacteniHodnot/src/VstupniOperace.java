// Knihovna VstupniOperace
public class VstupniOperace {
    public static void pouzijNacteniHodnot() {
        System.out.println("Používání třídy NacteniHodnot:");
        int hodnota = NacteniHodnot.nactiHodnoty(); // Volání metody třídy NacteniHodnot
        System.out.println("Zadali jste hodnotu: " + hodnota);
    }
}
