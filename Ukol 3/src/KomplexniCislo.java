class KomplexniCislo {
    private double realnaCast;
    private double imaginarniCast;

    public KomplexniCislo(double realnaCast, double imaginarniCast) {
        this.realnaCast = realnaCast;
        this.imaginarniCast = imaginarniCast;
    }

    public KomplexniCislo plus(KomplexniCislo cislo) {
        double novaRealnaCast = this.realnaCast + cislo.realnaCast;
        double novaImaginarniCast = this.imaginarniCast + cislo.imaginarniCast;
        return new KomplexniCislo(novaRealnaCast, novaImaginarniCast);
    }

    public KomplexniCislo plus(double cislo) {
        double novaRealnaCast = this.realnaCast + cislo;
        return new KomplexniCislo(novaRealnaCast, this.imaginarniCast);
    }

    public KomplexniCislo minus(KomplexniCislo cislo) {
        double novaRealnaCast = this.realnaCast - cislo.realnaCast;
        double novaImaginarniCast = this.imaginarniCast - cislo.imaginarniCast;
        return new KomplexniCislo(novaRealnaCast, novaImaginarniCast);
    }

    public KomplexniCislo minus(double cislo) {
        double novaRealnaCast = this.realnaCast - cislo;
        return new KomplexniCislo(novaRealnaCast, this.imaginarniCast);
    }

    public KomplexniCislo nasobek(KomplexniCislo cislo) {
        double novaRealnaCast = this.realnaCast * cislo.realnaCast - this.imaginarniCast * cislo.imaginarniCast;
        double novaImaginarniCast = this.realnaCast * cislo.imaginarniCast + this.imaginarniCast * cislo.realnaCast;
        return new KomplexniCislo(novaRealnaCast, novaImaginarniCast);
    }

    public KomplexniCislo nasobek(double cislo) {
        double novaRealnaCast = this.realnaCast * cislo;
        double novaImaginarniCast = this.imaginarniCast * cislo;
        return new KomplexniCislo(novaRealnaCast, novaImaginarniCast);
    }

    @Override
    public String toString() {
        return realnaCast + " + " + imaginarniCast + "i";
    }
}
