public class Main {
    public static void main(String[] args) {
        KomplexniCislo cislo1 = new KomplexniCislo(2.0, 3.0);
        KomplexniCislo cislo2 = new KomplexniCislo(1.5, 2.5);

        KomplexniCislo soucet =  cislo2.plus(cislo1);
        KomplexniCislo rozdil =  cislo2.minus(cislo1);
        KomplexniCislo nasobek =  cislo2.nasobek(cislo1);

        // Výpis výsledku operací
        System.out.println("Soucet: " + soucet);
        System.out.println("Soucet: " + rozdil);
        System.out.println("Nasobeni: " + nasobek);
    }
}
