import java.util.ArrayList;

// Enum for movement direction
enum SmerPohybu {
    LEFT, RIGHT, UP, DOWN
}

// Class to simulate movement on a chessboard
class Souradnice {
    private int x;
    private int y;

    public Souradnice(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "[" + x + ";" + y + "]";
    }
}

class HraPosunPoSachovnici {
    private int x;
    private int y;
    private int nx;
    private int ny;
    private ArrayList<Souradnice> historiePohybu;

    public HraPosunPoSachovnici(int nx, int ny) {
        this.nx = nx;
        this.ny = ny;
        this.x = 0;
        this.y = 0;
        this.historiePohybu = new ArrayList<>();
        this.historiePohybu.add(new Souradnice(0, 0));
    }

    public void provedPohyb(SmerPohybu smer) {
        switch (smer) {
            case LEFT:
                if (x > 0) {
                    x--;
                }
                break;
            case RIGHT:
                if (x < nx - 1) {
                    x++;
                }
                break;
            case UP:
                if (y > 0) {
                    y--;
                }
                break;
            case DOWN:
                if (y < ny - 1) {
                    y++;
                }
                break;
        }
        historiePohybu.add(new Souradnice(x, y));

        double vzdalenost = ToolHistoriePohybu.spoctiVzdalenost(historiePohybu);
        System.out.println("Vzdálenost: " + vzdalenost);
        String cesta = ToolHistoriePohybu.sestavVyslednouCestu(historiePohybu);
        System.out.println(cesta);
    }

    public String ziskejAktualniPolohu() {
        return "[" + x + ";" + y + "]";
    }
}
