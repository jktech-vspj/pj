public class Main {
    public static void main(String[] args) {
        BarvaRGB1 barvaRGB1 = new BarvaRGB1(255, 0, 0);
        System.out.println("Barva RGB1: " + barvaRGB1);
        System.out.println("Intenzita RGB1: " + barvaRGB1.intenzita);

        barvaRGB1.nastavSlozky(0, 255, 255);
        System.out.println("Upravená barva RGB1: " + barvaRGB1);
        System.out.println("Upravená barva RGB1 Intenzita: " + barvaRGB1.intenzita);

        BarvaRGB2 barvaRGB2 = new BarvaRGB2(0, 255, 0);
        System.out.println("Barva RGB2: " + barvaRGB2);
        System.out.println("Intenzita RGB2: " + barvaRGB2.intenzita);

        BarvaCMYK barvaCMYK = new BarvaCMYK(0, 50, 100, 0);
        System.out.println("Barva CMYK: " + barvaCMYK);
        System.out.println("Intenzita CMYK: " + barvaCMYK.intenzita);
    }
}