class Barva {
    protected int r;
    protected int g;
    protected int b;
    protected double intenzita;

    public Barva(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
        vypocetIntenzity();
    }

    public void nastavSlozky(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
        vypocetIntenzity();
    }

    public void vypocetIntenzity() {
        intenzita = (r + g + b) / 3.0;
    }

    @Override
    public String toString() {
        return String.format("#%02X%02X%02X", r, g, b);
    }
}

class BarvaRGB extends Barva {
    public BarvaRGB(int r, int g, int b) {
        super(r, g, b);
    }

    public void nastavSlozky(int r, int g, int b) {
        super.nastavSlozky(r, g, b);
    }
}

class BarvaRGB1 extends BarvaRGB {
    public BarvaRGB1(int r, int g, int b) {
        super(r, g, b);
    }

    public void nastavSlozky(int r, int g, int b) {
        super.nastavSlozky(r, g, b);
    }
}

class BarvaRGB2 extends BarvaRGB1 {
    public BarvaRGB2(int r, int g, int b) {
        super(r, g, b);
    }

    public void nastavSlozky(int r, int g, int b) {
        super.nastavSlozky(r, g, b);
    }
}

class BarvaCMYK extends Barva {
    private int c;
    private int m;
    private int y;
    private int k;

    public BarvaCMYK(int c, int m, int y, int k) {
        super(0, 0, 0);
        this.c = c;
        this.m = m;
        this.y = y;
        this.k = k;
        vypoctiRGB();
    }

    public void setCmyk(int c, int m, int y, int k) {
        this.c = c;
        this.m = m;
        this.y = y;
        this.k = k;
        vypoctiRGB();
    }

    private void vypoctiRGB() {
        r = (int) (255 * (1 - c / 100.0) * (1 - k / 100.0));
        g = (int) (255 * (1 - m / 100.0) * (1 - k / 100.0));
        b = (int) (255 * (1 - y / 100.0) * (1 - k / 100.0));
        vypocetIntenzity();
    }
}