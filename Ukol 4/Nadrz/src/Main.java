public class Main {
    public static void main(String[] args) {
        Nadrz nadrz = new Nadrz(100, "Benzín");

        try {
            nadrz.plnNadrz(50);
            System.out.println(nadrz.ziskejStavNadrze());

            nadrz.odeberZNadrze(30);
            System.out.println(nadrz.ziskejStavNadrze());

            nadrz.odeberZNadrze(40);
        }

        catch (MyException_PlnaNadrz | MyException_PrazdnaNadrz e) {
            System.out.println("Chyba: " + e.getMessage());
        }
    }
}
