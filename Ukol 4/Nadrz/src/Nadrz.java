// Výjimka pro přeplněnou nádrž
class MyException_PlnaNadrz extends Exception {
    public MyException_PlnaNadrz(String message) {
        super(message);
    }
}

// Výjimka pro prázdnou nádrž
class MyException_PrazdnaNadrz extends Exception {
    public MyException_PrazdnaNadrz(String message) {
        super(message);
    }
}

// Třída Nadrz
class Nadrz {
    private int stav;
    private int kapacita;
    private String typObsahu;

    public Nadrz(int kapacita, String typObsahu) {
        this.stav = 0;
        this.kapacita = kapacita;
        this.typObsahu = typObsahu;
    }

    public String ziskejStavNadrze() {
        double naplneni = (double) stav / kapacita * 100;
        return "Stav nádrže: " + stav + "/" + kapacita + " (" + String.format("%.2f", naplneni) + "%), typ: " + typObsahu;
    }

    public void plnNadrz(int mnozstvi) throws MyException_PlnaNadrz {
        if (stav + mnozstvi > kapacita) {
            throw new MyException_PlnaNadrz("Nelze přeplnit nádrž");
        }

        else {
            stav += mnozstvi;
        }
    }

    public void odeberZNadrze(int mnozstvi) throws MyException_PrazdnaNadrz {
        if (stav - mnozstvi < 0) {
            throw new MyException_PrazdnaNadrz("V nádrži není dostatek obsahu pro odebrání");
        } else {
            stav -= mnozstvi;
        }
    }
}