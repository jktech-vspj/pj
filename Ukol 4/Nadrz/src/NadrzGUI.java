import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NadrzGUI extends JFrame {
    private final Nadrz nadrz;
    private final JTextField textField;
    private JButton pridatButton;

    private final JLabel statusLabel;

    public NadrzGUI() {
        nadrz = new Nadrz(100, "Voda");
        setTitle("Nádrž GUI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 200);
        setLocationRelativeTo(null); // Center the window

        // Initialize components
        JLabel obsahLabel = new JLabel("Obsah: Voda");
        statusLabel = new JLabel("Status: 0");
        textField = new JTextField(15);
        //textField2 = new JTextField(15);
        pridatButton = new JButton("Přidat");
        JButton odebratButton = new JButton("Odebrat");

        // Create a panel to hold components
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        panel.add(new JLabel("Hodnota:"));
        panel.add(textField);
        panel.add(odebratButton);
        panel.add(pridatButton);

        pridatButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String input = textField.getText();
                int number;
                try {
                    number = Integer.parseInt(input);
                }

                catch (NumberFormatException ex) {
                    messageWindow("Neplatná hodnota");
                    return;
                }

                try {
                    nadrz.plnNadrz(number);
                }

                catch(MyException_PlnaNadrz exception) {
                    messageWindow(exception.getMessage());
                }

                statusLabel.setText(nadrz.ziskejStavNadrze());
            }
        });

        odebratButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String input = textField.getText();
                int number;
                try {
                    number = Integer.parseInt(input);
                }

                catch (NumberFormatException ex) {
                    messageWindow("Neplatná hodnota");
                    return;
                }

                try {
                    nadrz.odeberZNadrze(number);
                }

                catch(MyException_PrazdnaNadrz exception) {
                    messageWindow(exception.getMessage());
                }

                statusLabel.setText(nadrz.ziskejStavNadrze());
            }
        });

        // Add components to the frame
        setLayout(new BorderLayout());
        add(panel, BorderLayout.CENTER);
        add(obsahLabel, BorderLayout.SOUTH);
        add(statusLabel, BorderLayout.SOUTH);
    }

    private void messageWindow(String message) {
        JFrame newFrame = new JFrame("Nádrž GUI");
        JLabel label = new JLabel(message);
        newFrame.getContentPane().add(label);
        newFrame.setSize(300, 100);
        newFrame.setLocationRelativeTo(this); // Open the new window relative to the main window
        newFrame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                NadrzGUI window = new NadrzGUI();
                window.setVisible(true);
            }
        });
    }
}
