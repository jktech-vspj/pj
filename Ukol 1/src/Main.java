

public class Main {
    public static void main(String[] args) {

        System.out.println("Úkol I: ");
        datoveTypy.u_i();

        //System.out.println();
        System.out.println("\nÚkol II: ");
        datoveTypy.u_ii();

        System.out.println("\nÚkol III: ");
        String formatedName = datoveTypy.u_iii();
        System.out.println("Zformátované jméno: " + formatedName);

        System.out.println("\nÚkol IV: ");
        datoveTypy.u_iv_datovetypy();

        System.out.println("\nÚkol V: ");
        datoveTypy.u_v();

        char character = 'A';
        ZnakText.TiskInfoZnak(character); // Testování metody pro zobrazení informací o znaku

        ZnakText.tiskASCII(); // Testování metody pro vytisknutí ASCII tabulky
    }
}