import java.util.InputMismatchException;
import java.util.Scanner;

public class NacteniHodnot {

    private static Scanner scanner = new Scanner(System.in);

    public static Short nacti_short() {
        try {
            return scanner.nextShort();
        }

        catch (InputMismatchException e) {
            scanner.nextLine();
            return null;
        }
    }

    public static Integer nacti_int() {
        try {
            return scanner.nextInt();
        }

        catch (InputMismatchException e) {
            scanner.nextLine();
            return null;
        }
    }

    public static Float nacti_float() {
        try {
            return scanner.nextFloat();
        }

        catch (InputMismatchException e) {
            scanner.nextLine();
            return null;
        }
    }

    public static Double nacti_double() {
        try {
            return scanner.nextDouble();
        }

        catch (InputMismatchException e) {
            scanner.nextLine();
            return null;
        }
    }

    public static String nacti_String() {
        return scanner.nextLine();
    }

    public static Character nacti_char() {
        String input = scanner.nextLine();
        if (input.length() == 1) {
            return input.charAt(0);
        }

        else {
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println("Zadejte hodnotu typu short:");
        Short hodnotaShort = nacti_short();
        if (hodnotaShort == null) {
            System.out.println("Špatný formát údaje pro Short.");
        }

        System.out.println("Zadejte hodnotu typu int:");
        Integer hodnotaInt = nacti_int();
        if (hodnotaInt == null) {
            System.out.println("Špatný formát údaje pro Integer.");
        }

        System.out.println("Zadejte hodnotu typu float:");
        Float hodnotaFloat = nacti_float();
        if (hodnotaFloat == null) {
            System.out.println("Špatný formát údaje pro Float.");
        }

        System.out.println("Zadejte hodnotu typu double:");
        Double hodnotaDouble = nacti_double();
        if (hodnotaDouble == null) {
            System.out.println("Špatný formát údaje pro Double.");
        }

        System.out.println("Zadejte hodnotu typu String:");
        String hodnotaString = nacti_String();

        System.out.println("Zadejte hodnotu typu char:");
        Character hodnotaChar = nacti_char();
        if (hodnotaChar == null) {
            System.out.println("Špatný formát údaje pro Char.");
        }

        // Využití obalovací třídy (Wrapper class)
        int intValue = hodnotaInt != null ? hodnotaInt : 0; // Výchozí hodnota pro null

        System.out.println("Hodnota typu int: " + intValue);
    }
}
