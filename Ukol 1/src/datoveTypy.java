import java.util.Scanner;

public class datoveTypy {
    public static void u_i() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Zadej hodnotu: ");
        String input = scanner.nextLine();
        System.out.println("Zadanou hodnotu " + input + " lze uložit do datových typů: ");

        try {
            byte byteNumber = Byte.parseByte(input);
            System.out.println("Byte");
        }

        catch (NumberFormatException ignored) {}

        try {
            int intNumber = Integer.parseInt(input);
            System.out.println("Integer");
        }

        catch (NumberFormatException ignored) {}

        try {
            long longNumber = Long.parseLong(input);
            System.out.println("Long");
        }

        catch (NumberFormatException ignored) {}

        try {
            float floatNumber = Float.parseFloat(input);
            System.out.println("Float");
        }

        catch (NumberFormatException ignored) {}

        try {
            double doubleNumber = Double.parseDouble(input);
            System.out.println("Double");
        }

        catch (NumberFormatException ignored) {}

        System.out.println("String");
    }

    public static void u_ii() {
        final byte CONST_BYTE = 100;
        final int CONST_INT = 1000;
        final long CONST_LONG = 1000000000L;
        final float CONST_FLOAT = 10.5f;
        final double CONST_DOUBLE = 10.12345;
        final String CONST_STRING = "Linženýr Malinha";

        System.out.println("Byte: " + CONST_BYTE);
        System.out.println("Int: " + CONST_INT);
        System.out.println("Long: " + CONST_LONG);
        System.out.println("Float: " + CONST_FLOAT);
        System.out.println("Double: " + CONST_DOUBLE);
        System.out.println("String: " + CONST_STRING);
    }

    public static String u_iii() {
        final char SPACE_CHARACTER = ' ';

        Scanner scanner = new Scanner(System.in);

        System.out.print("Zadej jméno: ");
        String firstName = scanner.next();

        System.out.print("Zadej příjmení: ");
        String lastName = scanner.next();

        return firstName + SPACE_CHARACTER + lastName;
    }

    public static void u_iv_datovetypy() {
        System.out.println("| Datový typ\t| Obalovací třída\t| Velikost v paměti (bitech)\t| Velikost v paměti (bajtech) \t| Rozsah\t\t\t\t\t\t\t\t\t\t|");
        System.out.println("|_______________|___________________|_______________________________|_______________________________|_______________________________________________|");
        System.out.println("| byte\t\t\t| Byte\t\t\t\t| " + (Byte.BYTES * 8) + "\t\t\t\t\t\t\t\t| " + (Byte.BYTES) + "\t\t\t\t\t\t\t\t| " + Byte.MIN_VALUE + " až " + Byte.MAX_VALUE + "\t\t\t\t\t\t\t\t\t|");
        System.out.println("| int\t\t\t| Integer\t\t\t| " + (Integer.BYTES * 8) + "\t\t\t\t\t\t\t| " + (Integer.BYTES) + "\t\t\t\t\t\t\t\t| " + Integer.MIN_VALUE + " až " + Integer.MAX_VALUE  + "\t\t\t\t\t\t|");
        System.out.println("| long\t\t\t| Long\t\t\t\t| " + (Long.BYTES * 8) + "\t\t\t\t\t\t\t| " + (Long.BYTES) + "\t\t\t\t\t\t\t\t| " + Long.MIN_VALUE + " až " + Long.MAX_VALUE   + "\t|");
        System.out.println("| float\t\t\t| Float\t\t\t\t| " + (Float.BYTES * 8) + "\t\t\t\t\t\t\t| " + (Float.BYTES) + "\t\t\t\t\t\t\t\t| " + Float.MIN_VALUE + " až " + Float.MAX_VALUE   + "\t\t\t\t\t\t|");
        System.out.println("| double\t\t| Double\t\t\t| " + (Double.BYTES * 8) + "\t\t\t\t\t\t\t| " + (Double.BYTES) + "\t\t\t\t\t\t\t\t| " + Double.MIN_VALUE + " až " + Double.MAX_VALUE   + "\t\t\t|");
    }

    public static void u_v() {
        System.out.println("Výraz 6 + 4 = " + (6 + 4)); // Dochází ke sčítání dvou číslných hodnot
        System.out.println("Výraz \"\" + 6 + 4 = " + ("" + 6 + 4)); // Díky prázdnému řetězci dochází k ke sčítání stringů
    }
}
