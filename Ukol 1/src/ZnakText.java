public class ZnakText {

    public static void TiskInfoZnak(char z) {
        String info = DejInfoZnak(z);
        System.out.println(info);
    }

    public static String DejInfoZnak(char z) {
        int ascii = (int) z;
        String hex = Integer.toHexString(ascii);
        return "Znak: " + z + ", ASCII kód: " + ascii + ", Hexadecimální: " + hex.toUpperCase();
    }

    public static void tiskASCII() {
        System.out.println(DejASCII());
    }

    public static String DejASCII() {
        StringBuilder sb = new StringBuilder();
        sb.append("ASCII tabulka:\n");
        sb.append("Znak\t| Dec\t| Hex\t| Znak\t| Dec\t| Hex\t|\n");
        sb.append("________|_______|_______|_______|_______|_______|\n");
        for (char c = 'A'; c <= 'Z'; c++) {
            int asciiUpper = (int) c;
            int asciiLower = (int) Character.toLowerCase(c);
            sb.append(String.format("%c\t\t| %d\t| %s\t| %c\t\t| %d\t| %s\t|\n", c, asciiUpper, Integer.toHexString(asciiUpper).toUpperCase(), Character.toLowerCase(c), asciiLower, Integer.toHexString(asciiLower).toUpperCase()));
        }
        for (char c = '0'; c <= '9'; c++) {
            int ascii = (int) c;
            sb.append(String.format("%c\t\t| %d\t| %s\t|\n", c, ascii, Integer.toHexString(ascii).toUpperCase()));
        }
        return sb.toString();
    }
}
