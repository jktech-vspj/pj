package MatematickeFunkce;

public class Funkce_Primka extends Funkce {
    private double k; // Směrnice přímky
    private double q; // Posunutí

    // Konstruktor třídy Funkce_Primka
    public Funkce_Primka(double k, double q) {
        this.k = k;
        this.q = q;
    }

    // Metoda pro výpočet hodnoty y = kx + q
    @Override
    public double calc_y(double x) {
        return k * x + q;
    }

    // Metoda equals pro porovnání objektů
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Funkce_Primka)) {
            return false;
        }
        Funkce_Primka other = (Funkce_Primka) obj;
        return this.k == other.k && this.q == other.q;
    }

    // Metoda toString pro reprezentaci objektu ve formě textu
    @Override
    public String toString() {
        return "Funkce_Primka { k=" + k + ", q=" + q + " }";
    }
}