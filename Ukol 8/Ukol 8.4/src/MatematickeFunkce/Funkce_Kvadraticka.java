package MatematickeFunkce;

public class Funkce_Kvadraticka extends Funkce {
    private double a; // Parametr a kvadratické funkce
    private double b; // Parametr b kvadratické funkce
    private double c; // Parametr c kvadratické funkce

    // Konstruktor třídy Funkce_Kvadraticka
    public Funkce_Kvadraticka(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    // Metoda pro výpočet hodnoty y = ax^2 + bx + c
    @Override
    public double calc_y(double x) {
        return a * x * x + b * x + c;
    }

    // Metoda pro výpočet počtu kořenů kvadratické rovnice
    public int pocet_korenu() {
        double diskriminant = b * b - 4 * a * c;
        if (diskriminant > 0) {
            return 2;
        }

        else if (diskriminant == 0) {
            return 1;
        }

        else {
            return 0;
        }
    }

    // Metoda pro výpočet diskriminanty kvadratické rovnice
    public double diskriminant() {
        return b * b - 4 * a * c;
    }

    // Metoda equals pro porovnání objektů
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Funkce_Kvadraticka)) {
            return false;
        }
        Funkce_Kvadraticka other = (Funkce_Kvadraticka) obj;
        return this.a == other.a && this.b == other.b && this.c == other.c;
    }

    // Metoda toString pro reprezentaci objektu ve formě textu
    @Override
    public String toString() {
        return "Funkce_Kvadraticka { a=" + a + ", b=" + b + ", c=" + c + " }";
    }
}