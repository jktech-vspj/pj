import MatematickeFunkce.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Object> funkce = new ArrayList<>();

        Kolekce.addFunkce(funkce, new Funkce_Primka(1,2));
        Kolekce.addFunkce(funkce, new Funkce_Kvadraticka(1,2, 5));
        Kolekce.addFunkce(funkce, new Funkce_Primka(4,6));
        Kolekce.addFunkce(funkce, new Funkce_Kvadraticka(5,1,6));

        System.out.println("Všechny funkce");
        Kolekce.getAll(funkce);

        System.out.println("Všechny přímky");
        Kolekce.getPrimky(funkce);

        System.out.println("Kvadratické funkce");
        Kolekce.getKvadraticke(funkce);

        Kolekce.addFunkce(funkce, new Funkce_Kvadraticka(5,1,6));
    }
}