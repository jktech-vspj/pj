import MatematickeFunkce.Funkce_Kvadraticka;
import MatematickeFunkce.Funkce_Primka;

import java.util.ArrayList;

public class Kolekce {
    public static void getAll(ArrayList<Object> funkce) {
        for(Object fce : funkce) {
            System.out.println(fce);
        }
    }

    public static void getPrimky(ArrayList<Object> funkce) {
        for(Object fce : funkce) {
            if(fce instanceof Funkce_Primka) {
                System.out.println(fce);
            }
        }
    }

    public static void getKvadraticke(ArrayList<Object> funkce) {
        for(Object fce : funkce) {
            if(fce instanceof Funkce_Kvadraticka) {
                System.out.println(fce);
            }
        }
    }

    public static void addFunkce(ArrayList<Object> funkce, Object pridanaFunkce) {
        if(funkce.contains(pridanaFunkce)) {
            System.out.println("Funkce " + pridanaFunkce + " již v ArrayListu je");
        }

        else {
            funkce.add(pridanaFunkce);
        }
    }
}
