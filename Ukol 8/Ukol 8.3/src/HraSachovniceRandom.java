import java.util.Random;
import java.util.Scanner;

class HraSachovniceRandom extends HraSachovnice {
    private HraPosunPoSachovnici hra;

    public HraSachovniceRandom(int nx, int ny) {
        this.hra = new HraPosunPoSachovnici(nx, ny);
    }

    public void spustHru() {
        while (true) {
            tiskniAktualniPolohu();

            SmerPohybu[] allValues = SmerPohybu.values();

            Random random = new Random();
            int randomIndex = random.nextInt(allValues.length);
            SmerPohybu randomSmer = allValues[randomIndex];

            hra.provedPohyb(randomSmer);

            try {
                Thread.sleep(1000);
            }

            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void tiskniAktualniPolohu() {
        System.out.println("Aktuální poloha: " + hra.ziskejAktualniPolohu());
    }
}