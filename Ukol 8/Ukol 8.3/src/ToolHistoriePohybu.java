import java.util.ArrayList;

public class ToolHistoriePohybu {

    public static double spoctiVzdalenost(ArrayList<Souradnice> historie) {
        if (historie.size() < 2) {
            return 0.0;
        }

        Souradnice aktualni = historie.get(historie.size() - 1);

        return Math.sqrt(Math.pow(aktualni.getX(), 2) + Math.pow(aktualni.getY(), 2));
    }

    public static String sestavVyslednouCestu(ArrayList<Souradnice> historie) {
        StringBuilder cesta = new StringBuilder();

        for (int i = 0; i < historie.size(); i++) {
            Souradnice souradnice = historie.get(i);
            cesta.append("[").append(souradnice.getX()).append(";").append(souradnice.getY()).append("] ");
        }

        return cesta.toString();
    }
}