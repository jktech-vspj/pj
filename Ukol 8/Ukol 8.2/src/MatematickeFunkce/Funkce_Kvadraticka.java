package MatematickeFunkce;

public class Funkce_Kvadraticka extends Funkce {
    private double a; // Parametr a kvadratické funkce
    private double b; // Parametr b kvadratické funkce
    private double c; // Parametr c kvadratické funkce

    // Konstruktor třídy Funkce_Kvadraticka
    public Funkce_Kvadraticka(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    // Metoda pro výpočet hodnoty y = ax^2 + bx + c
    @Override
    public double calc_y(double x) {
        return a * x * x + b * x + c;
    }

    // Metoda pro výpočet počtu kořenů kvadratické rovnice
    public int pocet_korenu() {
        double diskriminant = diskriminant();
        if (diskriminant > 0) {
            return 2;
        }

        else if (diskriminant == 0) {
            return 1;
        }

        else {
            return 0;
        }
    }

    // Metoda pro výpočet diskriminanty kvadratické rovnice
    public double diskriminant() {
        return b * b - 4 * a * c;
    }

    // Metoda equals pro porovnání objektů
    public boolean equals(Funkce_Kvadraticka obj) {
        return this.a == obj.a && this.b == obj.b && this.c == obj.c;
    }

    // Metoda toString pro reprezentaci objektu ve formě textu
    @Override
    public String toString() {
        return "a = " + a + ", b = " + b + ", c = " + c;
    }
}