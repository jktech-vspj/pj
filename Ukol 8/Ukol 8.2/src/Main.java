import MatematickeFunkce.*;

public class Main {
    public static void main(String[] args) {
        // Testování třídy Funkce_Primka
        Funkce_Primka primka = new Funkce_Primka(2.0, 3.0);
        System.out.println("Hodnota y pro x = 2: " + primka.calc_y(2.0));

        // Testování třídy Funkce_Kvadraticka
        Funkce_Kvadraticka kvadraticka = new Funkce_Kvadraticka(1.0, -3.0, 2.0);
        System.out.println("Hodnota y pro x = 2: " + kvadraticka.calc_y(2.0));
        System.out.println("Počet kořenů pro kvadratickou funkci: " + kvadraticka.pocet_korenu());
        System.out.println("Diskriminant: " + kvadraticka.diskriminant());

        Funkce_Kvadraticka kvadraticka2 = new Funkce_Kvadraticka(1.0, 0.0, -4.0);
        System.out.println("Hodnota y pro x = 3: " + kvadraticka2.calc_y(3.0));
        System.out.println("Počet kořenů pro kvadratickou funkci: " + kvadraticka2.pocet_korenu());
        System.out.println("Diskriminant: " + kvadraticka2.diskriminant());
    }
}