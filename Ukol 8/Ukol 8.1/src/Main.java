public class Main {

    public static void main(String[] args) {
        // Příklad použití funkcí
        parseJmenoPrijmeni1("Jakub Hrdlička");
        parseJmenoPrijmeni2("   Ondřej   Linha  ");

        String[] logins = parseLogin("user1;user2;user3");
        for (String login : logins) {
            System.out.println("Login: " + login);
        }

        int[] cislaInt = parseCislaInt("1;2;3;4;5");
        for (int cislo : cislaInt) {
            System.out.println("Celé číslo: " + cislo);
        }

        double[] cislaDouble = parseCislaDouble("1.5;2.7;3.1");
        for (double cislo : cislaDouble) {
            System.out.println("Desetinné číslo: " + cislo);
        }
    }

    // Úkol 1a
    public static void parseJmenoPrijmeni1(String input) {
        String[] parts = input.split(" ");
        if (parts.length >= 2) {
            String jmeno = parts[0];
            String prijmeni = parts[1];
            System.out.println("Jméno: '" + jmeno + "', Příjmení: '" + prijmeni + "'");
        }

        else {
            System.out.println("Chybný formát pro získání jména a příjmení.");
        }
    }

    // Úkol 1b
    public static void parseJmenoPrijmeni2(String input) {
        String[] parts = input.trim().split("\\s+");
        if (parts.length >= 2) {
            String jmeno = parts[0];
            String prijmeni = parts[parts.length - 1];
            System.out.println("Jméno: '" + jmeno + "', Příjmení: '" + prijmeni + "'");
        }

        else {
            System.out.println("Chybný formát pro získání jména a příjmení.");
        }
    }

    // Úkol 1c
    public static String[] parseLogin(String input) {
        return input.split(";");
    }

    // Úkol 1d
    public static int[] parseCislaInt(String input) {
        String[] cislaStr = input.split(";");
        int[] cisla = new int[cislaStr.length];
        for (int i = 0; i < cislaStr.length; i++) {
            cisla[i] = Integer.parseInt(cislaStr[i]);
        }
        return cisla;
    }

    // Úkol 1e1
    public static double[] parseCislaDouble(String input) {
        String[] cislaStr = input.split(";");
        double[] cisla = new double[cislaStr.length];
        for (int i = 0; i < cislaStr.length; i++) {
            cisla[i] = Double.parseDouble(cislaStr[i]);
        }
        return cisla;
    }
}
