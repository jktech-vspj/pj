import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Mines {
    private int remaining;
    private JFrame frame;
    private JPanel panel;

    public Mines(int x, int y) {
        remaining = 0;

        if(x <= 0 || y <= 0) {
            displayFrame("error", "Neplatné hodnoty hracího pole");
            return;
        }

        buildGame(x,y);
    }

    private void buildGame(int x, int y) {
        int numberOfButtons = x * y;

        frame = new JFrame("Hra miny");
        panel = new JPanel(new GridLayout(x, y, 2, 2));

        createButtons(x * y);

        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createButtons(int numberOfButtons) {
        for (int i = 1; i <= numberOfButtons; i++) {
            JButton button = new JButton();
            button.setPreferredSize(new Dimension(40,40));
            button.setBackground(Color.GRAY);

            Random random = new Random();
            // Generate a random boolean value
            boolean randomBoolean = random.nextBoolean();

            button.putClientProperty("mine", randomBoolean);

            button.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Object clientValue = button.getClientProperty("mine");
                    boolean isMine = (boolean)clientValue;

                    if(isMine) {
                        remaining++;
                    }

                    if(isMine) {
                        displayFrame("Konec hry", "Kliknuli jste na pole s minou");
                    }

                    else {
                        remaining--;
                        button.setVisible(false);

                        if(remaining == 0) {
                            displayFrame("Konec hry", "Nalezli jste všechna správná pole");
                        }
                    }
                }
            });

            panel.add(button);
        }
    }

    private void displayFrame(String title, String text) {
        JFrame frame = new JFrame(title);
        JLabel label = new JLabel(text);

        frame.add(label);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
    }
}
