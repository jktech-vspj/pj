# Programování v Javě

### Popis
Vypracovaná cvičení do předmětu Programování v Javě na VŠPJ. Dokumentace k úkolům je sepsána v adresáři */Prezentace/Prezentace.docx*. 

### Použití
```git
git clone git@gitlab.com:jktech-vspj/pj.git
```

### Nástroje
- [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download)
